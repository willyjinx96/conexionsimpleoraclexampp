<?php
require_once "config.php";
$ci = $ci_err = "";
$nro_inv = $nombre = $es_jefe = $edad = "";

if (isset($_POST['ci']) && !empty(trim($_POST["ci"]))) {
  $_SESSION['ci'] = $_POST['ci'];
  $ci = trim($_POST["ci"]);
  $sql = "SELECT apellido||' '||nombre nombre, nroInv_supervisa(ci) nro_inv, es_jefe(ci) es_jefe, nroanios(fechanaci) edad FROM investigador WHERE ci = $ci";
  $result = $link->query($sql);
  if ($result) {
    if ($result->fetchColumn() > 0) {
      foreach ($link->query($sql) as $row) {
        $nombre = ($row['NOMBRE']);
        $nro_inv = ($row['NRO_INV']);
        $es_jefe =  ($row['ES_JEFE']);
        $edad =  ($row['EDAD']);
      }
      $result->closeCursor();
    } else {
      echo '<div class="alert alert-danger" role="alert">
  no existe el investigador con CI: ' . $ci . '</div>';;
    }
  }
} else {
  echo '<div class="alert alert-warning" role="alert">
  Ingrese un CI Valido</div>';;
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <title>Funciones ORACLE</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
  <style type="text/css">
    .wrapper {
      width: 650px;
      margin: 0 auto;
    }

    .page-header h2 {
      margin-top: 0;
    }

    table tr td:last-child a {
      margin-right: 15px;
    }
  </style>
  <script type="text/javascript">
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  </script>
</head>

<body>
  <div class="wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 text-center page-header">
          <h1 class="text-primary">FICHA - INVESTIGADOR</h1>
        </div>
        <div class="col-md-12">
          <form class="input-group" method="post">
            <input type="number" class="form-control" placeholder="CI de Investigador" name="ci" value="<?php echo $_SESSION['ci'] ?? '' ?>">
            <span class="input-group-btn">
              <input type="submit" class="btn btn-primary" name="info_inv" value="Buscar">
              <!--input type="reset" class="btn btn-danger" value="Borrar"-->
            </span>
          </form>
        </div>
        <div class="col-md-12">
          <div class="d-flex flex-column justify-content-between" style="padding: 40px;">
            <div class="input-group" style="padding-bottom: 10px;">
              <span class="input-group-addon" id="nombre">Apellidos y Nombres:</span>
              <input type="text" readonly class="form-control" id="nombre" aria-describedby="nombre" value="<?php echo $nombre; ?>">
            </div>
            <div class="input-group" style="padding-bottom: 10px;">
              <span class="input-group-addon" id="nro_inv">Nro. de Investigadores que supervisa:</span>
              <input type="text" readonly class="form-control" id="nro_inv" aria-describedby="nro_inv" value="<?php echo $nro_inv; ?>">
            </div>
            <div class="input-group" style="padding-bottom: 10px;">
              <span class="input-group-addon" id="es_jefe">Es Jefe de Departamento:</span>
              <input type="text" readonly class="form-control" id="es_jefe" aria-describedby="es_jefe" value="<?php echo $es_jefe; ?>">
            </div>
            <div class="input-group" style="padding-bottom: 10px;">
              <span class="input-group-addon" id="edad">Edad:</span>
              <input type="text" readonly class="form-control" id="edad" aria-describedby="edad" value="<?php echo $edad; ?>">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>