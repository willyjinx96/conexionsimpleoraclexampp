CREATE OR REPLACE FUNCTION nroInv_supervisa (xci IN NUMBER)
   RETURN NUMBER
IS
   xnum   NUMBER;
BEGIN
     SELECT COUNT (*) - 1
       INTO xnum
       FROM Investigador
      WHERE ci_supervisor = xci
   GROUP BY ci_supervisor;

   RETURN xnum;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 0;
END nroInv_supervisa;

CREATE OR REPLACE FUNCTION es_jefe (xci IN NUMBER)
   RETURN VARCHAR2
IS
   xnombre_d   VARCHAR2 (150);
BEGIN
   SELECT 'SI' || ' ' || nombre_d
     INTO xnombre_d
     FROM departamento
    WHERE ci_jefe = xci;

   RETURN xnombre_d;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN 'NO' || ' --- ';
END es_jefe;

CREATE OR REPLACE FUNCTION nroanios (xfecha IN DATE)
   RETURN NUMBER
IS
   edad   NUMBER;
BEGIN
   edad := TO_CHAR (SYSDATE, 'yyyy') - TO_CHAR (xfecha, 'yyyy');
   RETURN edad;
END nroanios;